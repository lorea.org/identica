<?php

/**
 * Elgg identi.ca view page
 *
 * @package ElggIdentica
 */

//some required params

$username = $vars['entity']->identica_username;
$num = $vars['entity']->identica_num;

// if the identi.ca username is empty, then do not show
if ($username) {
	elgg_load_js('friendly_time');
	elgg_load_js('identica');
?>

<div id="identica_widget">
	<ul id="identica_update_list" data-username="<?php echo $username; ?>" data-num="<?php echo $num; ?>"></ul>
	<p class="visit_identica"><a href="http://identi.ca/<?php echo $username; ?>"><?php echo elgg_echo("identica:visit"); ?></a></p>
</div>

<?php
} else {

	echo "<p>" . elgg_echo("identica:notset") . ".</p>";

}
