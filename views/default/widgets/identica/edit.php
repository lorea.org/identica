<?php

    /**
	 * Elgg identi.ca edit page
	 *
	 * @package ElggIdentica
	 */

?>
	<p>
		<?php echo elgg_echo("identica:username"); ?>
		<input type="text" name="params[identica_username]" value="<?php echo htmlentities($vars['entity']->identica_username); ?>" />	
		<br /><?php echo elgg_echo("twitter:num"); ?>
		<input type="text" name="params[identica_num]" value="<?php echo htmlentities($vars['entity']->identica_num); ?>" />	
	
	</p>
