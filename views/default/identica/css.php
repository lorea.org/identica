<?php 
/**
 * Elgg Identi.ca CSS
 * 
 * @package ElggIdentica
 */    
?>

#identica_widget {
	margin:0 10px 0 10px;
}
#identica_widget ul {
	margin:0;
	padding:0;
}
#identica_widget li {
	list-style-image:none;
	list-style-position:outside;
	list-style-type:none;
	margin:0 0 5px 0;
	padding:0;
	overflow-x: hidden;
	border: 2px solid #dedede;
	-webkit-border-radius: 8px;
	-moz-border-radius: 8px;
	border-radius: 8px;
}
#identica_widget li span {
	color:#666666;
	background:white;
	
	-webkit-border-radius: 8px; 
	-moz-border-radius: 8px;
	border-radius: 8px;
	
	padding:5px;
	display:block;
}
p.visit_identica a {
	background:url(<?php echo elgg_get_site_url(); ?>mod/identica/graphics/identica.png) left no-repeat;
	padding:0 0 0 20px;
	margin:0;
}
p.identica_username .input-text {
	width:200px;
}
.visit_identica {
	background:white;
	
	-webkit-border-radius: 8px; 
	-moz-border-radius: 8px;
	border-radius: 8px;
	
	padding:2px;
	margin:0 0 5px 0;
}
#identica_widget li > a {
	display:block;
	margin:0 0 0 4px;
}
#identica_widget li span a {
	display:inline !important;
}
