/**
 * Elgg identi.ca widget
 * This plugin allows users to pull in their identi.ca feed to display on their profile
 *
 * @package ElggIdentica
 */

elgg.identica = {};

/**
 * Parse the JSON result for show this in HTML
 *
 * @param   JSON string    the JSON result of the identi.ca's call
 */
elgg.identica.callback = function(identicas) {
  var statusHTML = [];
  for (var i=0; i<identicas.length; i++){
    var username = identicas[i].user.screen_name;
    var status = identicas[i].text.replace(/((https?|s?ftp|ssh)\:\/\/[^"\s\<\>]*[^.,;'">\:\s\<\>\)\]\!])/g, function(url) {
      return '<a href="'+url+'">'+url+'</a>';
    }).replace(/\B@([_a-z0-9]+)/ig, function(reply) {
      return  reply.charAt(0)+'<a href="http://identi.ca/'+reply.substring(1)+'">'+reply.substring(1)+'</a>';
    }).replace(/\B#([-_a-z0-9]+)/ig, function(tag) {
      return  tag.charAt(0)+'<a href="http://identi.ca/tag/'+tag.substring(1).replace(/[^a-z0-9]+/ig, '')+'">'+tag.substring(1)+'</a>';
    }).replace(/\B!([_a-z0-9]+)/ig, function(group) {
      return  group.charAt(0)+'<a href="http://identi.ca/group/'+group.substring(1)+'">'+group.substring(1)+'</a>';
    });
    statusHTML.push('<li><span>'+status+'</span> <a style="font-size:85%" href="http://identi.ca/notice/'+identicas[i].id+'">'+elgg.get_friendly_time(identicas[i].created_at)+'</a></li>');
  }
  document.getElementById('identica_update_list').innerHTML = statusHTML.join('');
}

elgg.identica.init = function() {
	username = $('#identica_update_list').data('username');
	num = $('#identica_update_list').data('num');
	js = "https://identi.ca/api/statuses/user_timeline/"+username+".json?callback=elgg.identica.callback&count="+num;
	$.getScript(js);
};

elgg.register_hook_handler('init', 'system', elgg.identica.init);
