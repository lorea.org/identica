<?php
/**
 * Identi.ca widget language file
 */

$english = array(

	'identica:title' => 'Identi.ca',
	'identica:info' => 'Display your latest tweets',
	'identica:username' => 'Enter your identi.ca username.',
	'identica:num' => 'The number of tweets to show.',
	'identica:visit' => 'visit my identi.ca',
	'identica:notset' => 'This Identi.ca widget is not yet set to go. To display your latest tweets, click on - edit - and fill in your details',
);

add_translation("en", $english);
