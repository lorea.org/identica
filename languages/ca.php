<?php
$language = array (
  'identica:title' => 'Identi.ca',
  'identica:info' => 'Mostra els teus últims missatges',
  'identica:username' => 'Introdueix el teu nom d\'usuària d\'identi.ca',
  'identica:num' => 'Número de missatges a mostrar.',
  'identica:visit' => 'Visitar el meu identi.ca',
  'identica:notset' => 'Aquest giny d\'Identi.ca no és encara llest. Per mostrar els teus últims missatges, clica a - edita - i omple amb els teus detalls',
);
add_translation("ca", $language);