<?php
$es = array (
  'identica:title' => 'Identi.ca',
  'identica:info' => 'Muestra tus últimos tweets',
  'identica:username' => 'Tu nombre de usuario en identi.ca',
  'identica:num' => 'Número de tweets a mostrar.',
  'identica:visit' => 'visita mi identi.ca',
  'identica:notset' => 'El widget de Identi.ca aún no está configurado. Para mostrar tus últimos tweets, haz click en en - editar - y rellena el formulario',
);

add_translation("es", $es);

