<?php
/**
 * Elgg identi.ca widget
 * This plugin allows users to pull in their identi.ca feed to display on their profile
 *
 * @package ElggIdentica
 */

elgg_register_event_handler('init', 'system', 'identica_init');

function identica_init() {
	elgg_extend_view('css/elgg', 'identica/css');
	elgg_register_js('identica', elgg_get_site_url() . "mod/identica/views/default/identica/js.php");
	elgg_register_js('friendly_time', elgg_get_site_url() . "mod/identica/views/default/js/friendly_time.php");
	elgg_register_widget_type('identica', elgg_echo('identica:title'), elgg_echo('identica:info'));
}
